#!/bin/bash
DRAWIO_BIN="drawio -x"

mkdir public/
find . -type f -name '*.drawio' -print0 | 
while IFS= read -r -d '' f; do
    path=${f%/*}
    base=${f##*/}
    basename=${base%.*}
    targetdir=$(echo public/${path:2} | sed 's/ /_/g' | tr '[:upper:]' '[:lower:]')
    # targetfile="$targetdir/$(echo $basename | sed 's/ /_/g' | tr '[:upper:]' '[:lower:]')"
    mkdir -p $targetdir
    
    if [ ! -f "$targetdir/$basename.png" ]; then
        $DRAWIO_BIN -f png --transparent -e -o "$targetdir" "$f" || true
    fi
    if [ ! -f "$targetdir/$basename.jpg" ]; then
        $DRAWIO_BIN -f jpg -o "$targetdir" "$f" || true
    fi
    if [ ! -f "$targetdir/$basename.svg" ]; then
        $DRAWIO_BIN -f svg -o "$targetdir" "$f" || true
    fi
done

    # $1 -x -f png 
    # cp --parents "$f" public
